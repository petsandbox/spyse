#!/usr/bin/env bash

if [ -z "$POSTGRES_PASSWORD" ] || [ -z "$POSTGRES_USER" ] || [ -z "$POSTGRES_DB" ]; then
    cat >&2<< EOF
****************************************************

VARIABLES:
POSTGRES_USER=$POSTGRES_USER
POSTGRES_DB=$POSTGRES_DB
POSTGRES_PASSWORD=$POSTGRES_PASSWORD

WARNING: No need variable has been set for the database.
     This will allow anyone with access to the
     Postgres port to access your database. In
     Docker's default configuration, this is
     effectively any other container on the same
     system.
     For example use  "-e POSTGRES_PASSWORD=password"
     to set it in "docker run".

****************************************************
EOF
    exit 1
fi

if [ -z "$1" ]; then
    SQL_FILE=/tmp/command.sql
    echo "Will execute file $SQL_FILE"
else
    SQL_FILE=$1
fi

psql -U $POSTGRES_USER -d $POSTGRES_DB -a -f $SQL_FILE




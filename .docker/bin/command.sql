CREATE TABLE IF NOT EXISTS t (id SERIAL not null primary key, jb jsonb);
COPY t (jb) from '/tmp/result.json';
CREATE INDEX t_jb_gin_idx ON t USING gin (jb jsonb_path_ops);
CREATE TABLE IF NOT EXISTS domain
(
    id serial not null constraint domain_pk primary key,
    domain character varying NOT NULL,
    parent_domain_id integer,
    ip character varying,
    CONSTRAINT domain_parent_domain_id_fkey FOREIGN KEY (parent_domain_id)
        REFERENCES domain (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
CREATE UNIQUE INDEX domain_domain_btree_idx ON domain USING btree (domain);

INSERT INTO domain (domain)
WITH  prepare AS (
SELECT  id, obj, regexp_replace(obj.value->>'name', '^.*\.', '') AS parent_domain FROM t
	JOIN LATERAL jsonb_array_elements(jb->'data'->'answers') obj(value) ON obj.value->>'type' = 'A'
	WHERE jb->'data'->'answers' @> '[{"type":"A"}]'
) SELECT DISTINCT parent_domain AS parent_domain  FROM prepare AS t1
LEFT JOIN
domain AS t2 ON t1.parent_domain = t2.domain WHERE t2.domain IS null;

INSERT INTO domain (domain, parent_domain_id, ip)
WITH  prepare AS (
SELECT  id, obj, regexp_replace(obj.value->>'name', '^.*\.', '') as parent_domain FROM t
	JOIN  LATERAL jsonb_array_elements(jb->'data'->'answers') obj(value) ON obj.value->>'type' = 'A'
	WHERE jb->'data'->'answers' @> '[{"type":"A"}]'
) SELECT obj->>'name' AS site_domain, t2.id AS parent_id,  obj->>'answer' AS ip  FROM prepare AS t1
JOIN domain AS t2 ON t1.parent_domain = t2.domain
ON CONFLICT (domain) DO  NOTHING;

DROP TABLE IF EXISTS t;
# SpySE

## docker-postgres-pgAdmin
Docker Compose configuration to run PostgreSQL 11 and pgAdrmin 4 .


## Install prerequisites

* [Docker CE](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install)
* Git (optional)

## How to use it

### Starting Docker Compose

Checkout the repository or download the sources.

Simply run `docker-compose up --buid -d` and you are done.

pgAdmin will be available on `localhost:8080` and PostgreSQL on `localhost:5432`.

### Using Composer

`docker-compose run composer <cmd>`

Where `cmd` is any of the available composer command.

### Using PostgreSQL

Default connection:

`docker-compose exec db psql -U postgres`

Using .env file default parameters:

`docker-compose exec db psql -U dbuser spy`

If you want to connect to the DB from another container (from the `php` one for instance), the host will be the service name: `db`.


### Configuring PostgreSQL

If you want to change the db name, db user and db password simply edit the `.env` file at the project's root.

If you connect to PostgreSQL from localhost a password is not required however from another container you will have to supply it.

## Execute command: 
### Start docker
```
$ docker-compose up --buid -d
```
### Init and execute sql
```
$ docker exec -it spy_db /tmp/init.sh 
```

### It's all. You can see the result in pgAdmin DB `spy` table `domain`
`http://localhost:8080`  
login `user@domain.com`  
pass  `dbpwd`
#### Connect to DB in pgAdmin  
* Add new server  
* Name: my_connect_name
#### Tab Connection:
* Host name/address: `db`  
* Maintenance db: `spy`
* Username: `dbuser`
* Password: `dbpwd`  

#### Init db :
````
./.docker/bin/init.sh
````
#### Parsing code:
````
./.docker/bin/command.sql
````
### Save Data DB and pgAdmin settings on Host machine
If you want to save DB and pgAdmin settings on Host machine,  
you have edit `docker-compose.yml` file.   
Uncomment 'volumes' settings.     